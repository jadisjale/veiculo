<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'board', 'pattern', 'brand', 'year_fabrication', 'year_model', 'km_init', 'km_now', 'user_id'
    ];

}
