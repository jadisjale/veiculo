<?php

namespace App\Http\Controllers;

use App\Service\VehicleService;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    private $vehicleService;
    //
    function __construct()
    {
        $this->vehicleService = new VehicleService();
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->vehicleService->save($request);
    }

    public function findByBoard(Request $request)
    {
        return $this->vehicleService->findByBoard($request);
    }

    public function photo(Request $request)
    {
        return $this->vehicleService->photo($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function listVehicle()
    {
        return $this->vehicleService->listVehicleByClient();
    }

    public function viewFile($id_vehicle)
    {
        $photos = $this->vehicleService->getVehicles($id_vehicle);
        $vehicle = DB::table('vehicles')
            ->where('id', $id_vehicle)
            ->first();
        return view('view_file', ['photos' => $photos, 'vehicle' => $vehicle]);
    }

    public function vehicleUpdate($id) {
        $vehicle = $this->vehicleService->findVehicle($id);
        return view('vehicle', ['vehicle' => $vehicle]);
    }
}
