<?php

namespace App\Http\Controllers;

use App\Establishment;
use App\Iten;
use App\Service\BoardService;
use App\Service\EstablishmentService;
use App\Service\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewController extends Controller
{

//    function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function client()
    {
        return view('client');
    }

    public function vehicle()
    {
        return view('vehicle');
    }

    public function listVehicle()
    {
        return view('vehicle_list');
    }

    public function teste()
    {
        return view('teste');
    }

    public function vehiclePhoto($id_vehicle)
    {
        $vehicle = DB::table('vehicles')
            ->where('id', $id_vehicle)
            ->first();

        $photos = DB::table('photos')
            ->where('vehicle_id', $id_vehicle)
            ->get();

        return view('vehicle_photo', ['vehicle' => $vehicle, 'photos' => $photos]);
    }
}
