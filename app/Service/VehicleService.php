<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Photo;
use App\Vehicle;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;


class VehicleService extends ServiceController implements InterfaceServiceController
{

    private $repository = DIRECTORY_SEPARATOR . "file" . DIRECTORY_SEPARATOR;
    private $vehicle;
    private $arrayValidation = array();
    private $MSG_UPDATE_STATUS = ' ID INVÁLIDO ';
    private $MSG_UPDATE_PRICE = ' PREÇO ATUALIZADO COM SUCESSO! ';
    private $MSG_UPDATE_PRICE_ELSE = ' NÃO FOI POSSÍVEL INDENTIFICAR O ID DA VENDA! ';
    private $MSG_EXCEPTION_STATUS = ' ERRO AO ALTERAR STATUS DO PEDIDO ';

    function __construct()
    {
        $this->vehicle = new Vehicle();
    }

    public function findByBoard(Request $request) {
        try {
            return $this->vehicle->where('board', $request->input('board'))->get();
        } catch (\Exception $e) {
            return "Erro ao consultar placa";
        }
    }

    private function createRepository($user_id, $vehicle_id) {
        if (empty($user_id) || empty($vehicle_id)) {
            throw new \Exception("Erro nos parâmetros");
        }
        $dir = DIRECTORY_SEPARATOR . 'upload/' . $user_id . "_". $vehicle_id;
        $this->rmdir($dir);
        $photo = new Photo();
        $photo->where('user_id', $user_id)
            ->where('vehicle_id', $vehicle_id)
            ->delete();
        if(!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        return $dir;
    }

    private function rmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir"){
                        rmdir($dir."/".$object);
                    }else{
                        unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function photo(Request $request) {
        try {
            DB::beginTransaction();
            $user_id = $request->input('client_id');
            $vehicle_id = $request->input('vehicle_id');
            $files = $request->file('image');
            if ($files) {
                $dir = $this->createRepository($user_id, $vehicle_id);
                foreach ($files as $file) {
                    $path = $dir . '\\' . $file->getClientOriginalName();
                    rename($file->path(),  $path);
                    $photo = new Photo();
                    $photo->user_id = $user_id;
                    $photo->vehicle_id = $vehicle_id;
                    $photo->path = $path;
                    $photo->save();
                }
                DB::commit();
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, "Cadastrado com sucesso");
            }
            return $this->returnJson($this->codeError, $this->messageError, 'Não foi encontrado imagens');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->returnJson($this->codeError, $this->messageError, $e->getMessage());
        }
     }

    public function save(Request $request)
    {
        try {

            $this->vehicle->board = $request->input('board');
            $this->vehicle->pattern = $request->input('pattern');
            $this->vehicle->brand = $request->input('brand');
            $this->vehicle->year_fabrication = $request->input('year_fabrication');
            $this->vehicle->year_model = $request->input('year_model');
            $this->vehicle->km_init = $request->input('km_init');
            $this->vehicle->km_now = $request->input('km_now');
            $this->vehicle->user_id = $request->input('user_id');

            if (!$this->validation()){
                $this->vehicle->save();
                return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->vehicle);
            } else {
                return $this->returnJson($this->codeWarning, $this->validation, $this->arrayValidation);
            }
        } catch (\Exception $exception) {
            if ($exception->getCode() === "23505") {
                return $this->returnJson($this->codeInfo, $this->messageInfo, "Provavelmente essa placa já existe " . $this->vehicle->board);
            }
            return $this->returnJson($this->codeError, $this->messageError, $exception->getMessage());
        }
    }

    public function update(Request $request, $id)
    {

    }

    public function remove(Request $request, $sell_id)
    {

    }

    public function findAll(Request $request){
        try {
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->client->all());
        } catch (Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_FIND_ALL);

        }
    }

    public function findPk(Request $request, $id)
    {
        try {
            $result = $this->client->where('id', $id)->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_FIND_PK);
        }

    }

    public function listVehicleByClient()
    {
        try {
            $result = $this->vehicle->where('user_id', 1)->get();
            return $this->returnJson($this->codeSuccess, $this->messageSuccess, $result);
        } catch (\Exception $e) {
            return $this->returnJson($this->codeError, $this->messageError, $this->MSG_EXCEPTION_FIND_PK);
        }
    }

    public function validation()
    {
        $validator = Validator::make(
            array(
                'board' => $this->vehicle->board,
                'pattern' => $this->vehicle->pattern,
                'brand' => $this->vehicle->brand,
                'user_id' => $this->vehicle->user_id,
            ),
            array(
                'board' => 'required',
                'pattern' => 'required',
                'brand' => 'required',
                'user_id' => 'required',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }
}