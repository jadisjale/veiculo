<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 24/02/2017
 * Time: 18:50
 */

namespace App\Service;


use App\Photo;
use App\Vehicle;
use League\Flysystem\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;


class PhotoService extends ServiceController
{

    private $arrayValidation = array();

    public function save(Photo $photo) {
        try {
            if (!$this->validation($photo)){
                $photo->save();
            } else {
                throw new \Exception("Erro na validação das imagens");
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function validation($photo)
    {
        $validator = Validator::make(
            array(
                'user_id' =>  $photo->user_id,
                'vehicle_id' => $photo->vehicle_id,
                'path' => $photo->path,
            ),
            array(
                'user_id' => 'required',
                'vehicle_id' => 'required',
                'path' => 'required',
            )
        );

        if ($validator->fails())
        {
            $messages = $validator->messages();
            foreach ($messages->all() as $message)
            {
                array_push($this->arrayValidation, $message);
            }
        }

        return $this->arrayValidation ? true : false;
    }

}