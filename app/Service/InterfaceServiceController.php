<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 22/07/2017
 * Time: 07:21
 */

namespace app\Service;
use Illuminate\Http\Request;

interface InterfaceServiceController
{
    public function save(Request $request);
    public function update(Request $request, $id);
    public function remove(Request $request, $id);
    public function findAll(Request $request);
    public function findPk(Request $request, $id);
    public function validation();
}