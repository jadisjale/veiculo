<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    protected $fillable = [
        'id', 'vehicle_id', 'description', 'date_change'
    ];
}
