@extends('template')

@section('title', 'Vehicle')

@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>Cadastrar Veículo</h2>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Criar um novo veículo</h2>
                </div>
                <div class="body">
                    <form id="form-save-vehicle" method="POST" autocomplete="off" enctype="multipart/form-data">
                        <input type="hidden" name="user_id" value="1">
                        <input type="hidden" name="id" value="{{ @$vehicle->id }}">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="board" id="board" value="{{ @$vehicle->board }}">
                                <label class="form-label">Placa</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="brand" id="brand" value="{{ @$vehicle->brand }}">
                                <label class="form-label">Marca</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="pattern" id="pattern" value="{{ @$vehicle->pattern }}">
                                <label class="form-label">Modelo</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control year" name="year_fabrication" id="year_fabrication" value="{{ @$vehicle->year_fabrication }}">
                                <label class="form-label">Ano Fabricação</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control year" name="year_model" id="year_model" value="{{ @$vehicle->year_model }}">
                                <label class="form-label">Ano Modelo</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="km_init" id="km_init" value="{{ @$vehicle->km_init }}">
                                <label class="form-label">Km Inicial</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="km_now" id="km_now" value="{{ @$vehicle->km_now }}">
                                <label class="form-label">Km Atual</label>
                            </div>
                        </div>
                        <button class="btn btn-primary waves-effect" type="submit">Cadastrar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="/vendors/plugins/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            vehicle.index();
        });
    </script>

@endsection