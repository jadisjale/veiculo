@extends('template')

@section('title', 'Client')

@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>Cadastrar Cliente</h2>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Criar um novo cliente</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"><a href="javascript:void(0);"
                                                class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false"> <i
                                        class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul></li>
                    </ul>
                </div>
                <div class="body">
                    <form id="form-save-client" method="POST" autocomplete="off" enctype="multipart/form-data">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" value="teste" id="name">
                                <label class="form-label">Nome</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" name="email" value="teste@teste.com" id="email">
                                <label class="form-label">Email</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="login" autocomplete="off" value="jsj" id="login">
                                <label class="form-label">Login</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" autocomplete="off" value="123" id="senha"
                                > <label class="form-label">Senha</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <div class="file-loading">
                                    <input id="file" name="teste" type="file" accept="image/*">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary waves-effect" type="submit">Submeter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection