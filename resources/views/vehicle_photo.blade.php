@extends('template')

@section('title', 'Vehicle')

@section('content')

    <link href="/vendors/plugins/light-gallery/css/lightgallery.css" rel="stylesheet">
    <link href="/js/custom/import/toastr.min.css" rel="stylesheet" />


    <script src="/vendors/plugins/jquery/jquery.min.js"></script>
    <script src="/vendors/plugins/light-gallery/js/lightgallery-all.js"></script>
    <script src="/vendors/js/pages/medias/image-gallery.js"></script>

    <div class="container-fluid">
        <div class="block-header">
            <h2>Cadastrar Veículo - Adicionar Fotos</h2>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-light-blue">
                    <h2>
                        Fotos do veículo <small>Confira as fotos...</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="timer" data-loading-color="lightBlue">
                                <i class="material-icons">loop</i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                        @for ($i = 0; $i < count($photos); $i++)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <a href="{{$photos[$i]->path}}" data-sub-html="Foto {{$i+1}}">
                                    <img class="img-responsive thumbnail" src="{{$photos[$i]->path}}" height="250" width="250">
                                </a>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2>Adicionar Fotos</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"><a href="javascript:void(0);"
                                                class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false"> <i
                                        class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul></li>
                    </ul>
                </div>
                <div class="card">
                    <div class="header bg-light-blue">
                        <h2>
                            Dados do veículo <small>A baixo uma breve descrição...</small>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li>
                                <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="timer" data-loading-color="lightBlue">
                                    <i class="material-icons">loop</i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <ul style="list-style-type:disc">
                            <li> <b>Placa:</b> {{ $vehicle->board }} </li>
                            <li> <b>Marca:</b> {{ $vehicle->brand }} </li>
                            <li> <b>Modelo:</b> {{ $vehicle->pattern }} </li>
                        </ul>

                        <form id="form-save-vehicle-photo" method="POST" autocomplete="off" enctype="multipart/form-data">
                            <input type="hidden" name="user_id"  value="1">
                            <input type="hidden" name="id_vehicle" value="{{ $vehicle->id }}">
                            <input type="file" name="photos[]" id="file-photo" accept="image/*" multiple>
                            <br>
                            <button class="btn btn-primary waves-effect" type="submit">Cadastrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/vendors/plugins/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            vehicle.validadorPhoto();
            $("#file-photo").fileinput({
                maxFileCount: 5,
                language: "pt-BR",
                browseClass: "btn btn-primary btn-block",
                showCaption: false,
                showRemove: false,
                showUpload: false
            });
        });
    </script>

@endsection