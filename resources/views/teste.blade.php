@extends('template')

@section('title', 'Client')

@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>Cadastrar Cliente</h2>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Criar um novo cliente</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"><a href="javascript:void(0);"
                                                class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false"> <i
                                        class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul></li>
                    </ul>
                </div>
                <div class="body">
                    <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="https://www.wikihow.com/images_en/thumb/4/46/Shorten-a-URL-Step-1-Version-3.jpg/v4-728px-Shorten-a-URL-Step-1-Version-3.jpg" data-sub-html="Demo Description">
                                <img class="img-responsive thumbnail" src="https://www.wikihow.com/images_en/thumb/4/46/Shorten-a-URL-Step-1-Version-3.jpg/v4-728px-Shorten-a-URL-Step-1-Version-3.jpg">
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="http://www.fundosanimais.com/1920x1080/imagens-tigres.jpg" data-sub-html="Demo Description">
                                <img class="img-responsive thumbnail" src="http://www.fundosanimais.com/1920x1080/imagens-tigres.jpg">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/vendors/plugins/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#aniimated-thumbnials').lightGallery({
                thumbnail: true,
                selector: 'a'
            });
        });
    </script>

@endsection