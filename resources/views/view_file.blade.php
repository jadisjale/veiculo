@extends('template')

@section('title', 'Client')

@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>Fotos dos veículo</h2>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-light-blue">
                    <h2>
                        Dados do veículo <small>A baixo uma breve descrição...</small>
                    </h2>
                </div>
                <div class="body">
                    <ul style="list-style-type:disc">
                        <li> <b>Placa:</b> {{ $vehicle->board }} </li>
                        <li> <b>Marca:</b> {{ $vehicle->brand }} </li>
                        <li> <b>Modelo:</b> {{ $vehicle->pattern }} </li>
                    </ul>
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2>Confira as fotos do veículo</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"><a href="javascript:void(0);"
                                                class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false"> <i
                                        class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="/vehicle/photo/{{$photos[0]->vehicle_id}}">Adicionar novas fotos</a></li>
                            </ul></li>
                    </ul>
                </div>
                <div class="body">
                    <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                    @foreach($photos as $photo)
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="{{ $photo->path }}" data-sub-html="{{ $photo->name }}">
                                <img class="img-responsive thumbnail" src="{{ $photo->path }}">
                            </a>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/vendors/plugins/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#aniimated-thumbnials').lightGallery({
                thumbnail: true,
                selector: 'a'
            });
        });
    </script>

@endsection