@extends('template')

@section('title', 'Vehicle')

@section('content')

    <div class="container-fluid">
        <div class="block-header">
            <h2>Lista de veículos</h2>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Lista de veículos</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown"><a href="javascript:void(0);"
                                                class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                aria-haspopup="true" aria-expanded="false"> <i
                                        class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul></li>
                    </ul>
                </div>
                <div class="body">
                    <table id="list_vehicles" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>Placa</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Ano Fabricação</th>
                            <th>Informações</th>
                            <th width="10px">Fotos</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="/vendors/plugins/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function () {
            vehicle.indexList.index();
        });
    </script>

@endsection