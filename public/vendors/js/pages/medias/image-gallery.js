$(function () {
    $('#aniimated-thumbnials').lightGallery({
        thumbnail: true,
        selector: 'a',
        animateThumb: true,
        showThumbByDefault: true,
    });
});