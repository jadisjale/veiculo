﻿/**
 * Created by jsj on 27/05/2017.
 */
var main = {

    idJogador: '',
    formUpdateRaxa: $('#update_raxa'),

    index: function () {
        main.verificUsuarioLogado();
        main.countJogador();
        main.countEnquete();
        main.destruieSessao();
        main.loadModalResultado();
        main.validFormUpdateRaxa();
        main.modalSaveNewRaxa();
        main.validFormSaveRaxa();
        main.removeRaxa();
        main.updateTableListRaxa();
        main.setTheme();
    },

    countJogador: function () {
        tools.ajax(url.countJogador, null, url.post, function (result) {
            $('#count_jogador').html(result);
        });
    },

    countEnquete: function () {
        tools.ajax(url.countEnquete, null, url.post, function (result) {
            $('#count_enquete').html(result);
        });
    },

    sumGolsByJogador: function () {
        var data = {'jogador': main.idJogador};
        tools.ajax(url.sumGol, data, url.post, function (result) {
            $('#sum_gol').html(result);
        });
    },

    sumVitoriaByJogador: function () {
        var data = {'jogador': main.idJogador};
        tools.ajax(url.sumVitoria, data, url.post, function (result) {
            $('#sum_vitorias').html(result);
        });
    },
    
    getJogadores: function () {
        var data = {'id': main.idJogador};
        console.log(data);
        tools.ajax(url.getJogadores, data, url.post, function (result) {
            console.log(result);
        });
    },

    verificUsuarioLogado: function () {
        tools.ajax(url.usuario_logado, null, url.post, function (result) {
            if (result === false) {
                window.location.href = window.location.protocol+"//"+window.location.hostname+'/raxa_web/';
            } else {
                main.idJogador = parseInt(result);
                main.sumGolsByJogador();
                main.sumVitoriaByJogador();
                main.initTableRaxas();
                main.getJogadorDetails();
            }
        });
    },

    destruieSessao: function () {
        $('#logout').click(function () {
            tools.ajax(url.destroy_sessao, null, url.post, function (result) {
                window.location.href = window.location.protocol+"//"+window.location.hostname+'/raxa_web/home.html';
            });
        });
    },

    getJogadorDetails: function () {
        tools.ajax(url.get_jogador_details+'?id=' + main.idJogador, null, url.get, function (result) {
            var json = JSON.parse(result.responseText.trim());
            var item = json.objetos[0];
            jogador.id = item.id;
            jogador.nome = item.nome;
            jogador.file = item.file;
            jogador.setValuesJogador();
        });
    },

    initTableRaxas: function () {
        var table = $('#resultados_jogos').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": url.getRaxaByJogador+'?jogador='+main.idJogador,
                "dataSrc": "raxa"
            },
            "columns": [
                { "data": "dia" },
                { "data": "gol"},
                { "data": "vitoria"},
                { "data": "id"}
            ],
            "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });

        $('#resultados_jogos tbody').on( 'click', 'tr', function () {
            var data = table.row( this ).data();
            main.setValuesModalUpdateRaxa(data);
            $('#edit-raxa').modal('show');
        });
    },

    setValuesModalUpdateRaxa: function (data) {
        $('#dia').val(data.dia);
        $('#id').val(data.id);
        $('#jogador').val(data.jogador);
        $('#gol').val(data.gol);
        $('#golContra').val(data.golContra);
        $('#vitoria').val(data.vitoria);
        $('#derrota').val(data.derrota);
    },

    validFormUpdateRaxa: function () {
        $('#update_raxa').validate({
            rules: {
                id: {
                    required: true
                },
                jogador: {
                    required: true
                },
                dia: {
                    required: true
                },
                gol: {
                    required: true
                },
                golContra: {
                    required: true
                },
                vitoria: {
                    required: true
                },
                derrota: {
                    required: true
                }
            },
            submitHandler: function() {
                main.updateRaxa();
                return false;
            }
        });
    },

    updateRaxa: function () {
        tools.ajax(url.update_raxa, main.formUpdateRaxa.serialize(), url.post, function (result) {
            if(result.success === 1) {
                tools.messageToast('success', result.message);
                tools.resetForm('update_raxa');
                main.updateListRaxa();
                $('#edit-raxa').modal('hide');
            } else {
                tools.messageToast('error', 'Houve um erro ao tentar atualizar dados');
            }
        });
    },

    loadModalResultado: function () {
        $('.loadModalResultado').click(function (e) {
            e.preventDefault();
            var action = $(this).attr('href');
            main.getResultAction(action);
        });
    },

    getResultAction:function (action) {
        var caminho = 'http://webfate.esy.es/raxa/';
        tools.ajaxAsyncFalse(caminho+action, null, url.post, function (result) {
            $('#modalResult').modal('show');
            main.labelModalResult(result.raxas, action);
        });
    },

    labelModalResult: function (result, action) {
        switch(action) {
            case 'get_rank_vitoria.php':
                main.setValuesModalResult('Quem mais venceu', result[0].nome, result[0].vitoria, result[1].nome, result[1].vitoria, result[2].nome, result[2].vitoria);
                break;
            case 'get_rank_gol.php':
                main.setValuesModalResult('Quem fez mais gol', result[0].nome, result[0].gols, result[1].nome, result[1].gols, result[2].nome, result[2].gols);
                break;
            case 'get_rank_jogos.php':
                main.setValuesModalResult('Quem mais jogou', result[0].nome, result[0].ids, result[1].nome, result[1].ids, result[2].nome, result[2].ids);
                break;
            case 'get_rank_negativo_jogo.php':
                main.setValuesModalResult('Quem menos jogou', result[0].nome, result[0].ids, result[1].nome, result[1].ids, result[2].nome, result[2].ids);
                break;
            case 'get_rank_negativo_gol.php':
                main.setValuesModalResult('Quem menos fez gol', result[0].nome, result[0].gols, result[1].nome, result[1].gols, result[2].nome, result[2].gols);
                break;
            case 'get_rank_negativo_vitoria.php':
                main.setValuesModalResult('Quem menos venceu', result[0].nome, result[0].vitorias, result[1].nome, result[1].vitorias, result[2].nome, result[2].vitorias);
            break;
        }
    },

    setValuesModalResult: function (title, primeiro, valorPrimeiro, segundo, valorSegundo, terceiro, valorTerceiro) {
        $('#title-modal').text(title);
        $('#primeiro').text(primeiro);
        $('#result-primeiro').text(valorPrimeiro);
        $('#segundo').text(segundo);
        $('#result-segundo').text(valorSegundo);
        $('#terceiro').text(terceiro);
        $('#result-terceiro').text(valorTerceiro);
    },

    modalSaveNewRaxa: function () {
        $('a').click(function () {
            if ( $(this).hasClass('float')) {
                $('#save-raxa').modal('show');
                $('#dia_save').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });
                $('#dia').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });
            }
        });
    },

    validFormSaveRaxa: function () {
        $('#save_raxa').validate({
            rules: {
                jogador: {
                    required: true
                },
                dia: {
                    required: true
                },
                gol: {
                    required: true
                },
                golContra: {
                    required: true
                },
                vitoria: {
                    required: true
                },
                derrota: {
                    required: true
                }
            },
            submitHandler: function() {
                main.saveRaxa();
                return false;
            }
        });
    },

    saveRaxa: function () {
        var data = {
            'jogador'   : main.idJogador,
            'dia'       : $('#dia_save').val(),
            'gol'       : $('#gol_save').val(),
            'golContra' : $('#golContra_save').val(),
            'vitoria'   : $('#vitoria_save').val(),
            'derrota'   : $('#derrota_save').val()
        };

        tools.ajax(url.create_raxa, data, url.post, function (result) {
            if (result.success === 1) {
                tools.messageToast('success', 'Raxa inserido com sucesso!');
                $('#save-raxa').modal('hide');
                tools.resetForm('save_raxa');
                main.updateListRaxa();
            } else {
                tools.messageToast('error', 'Erro ao inserir raxa');
            }
        });
    },

    removeRaxa: function () {
        var data = {'id': $('#id').val()};
        if ($('#id').val() !== ''){
            tools.ajax(url.delete_raxa, data, url.post, function (result) {
                console.log(result);
                if (result.success === 1) {
                    $('#edit-raxa').modal('hide');
                    tools.resetForm('update_raxa');
                    main.updateListRaxa();
                } else {
                    tools.messageToast('error', 'Erro ao remover raxa');
                }
            });
        }
    },

    updateListRaxa: function () {
        $('#resultados_jogos').DataTable().ajax.reload();
    },

    updateTableListRaxa: function () {
        $('.atualizar-raxas').click( function () {
            $('#resultados_jogos').DataTable().ajax.reload();
        });
    },

    setTheme: function () {
        $('body').addClass('theme-' + 'indigo');
    }

};
