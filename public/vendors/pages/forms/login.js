/**
 * Created by fabricainfo on 31/05/17.
 */
var login = {

    formLogin: $('#login'),
    formLoginUp: $('#sign_up'),

    sigin: function () {
        tools.ajax(url.sigIn, login.formLogin.serialize(), url.post, function (result) {
            if(result.hasOwnProperty('pessoas')) {
                window.location.href = window.location.protocol+"//"+window.location.hostname+'/raxa_web/home.html';
            } else {
                tools.messageToast('error', 'Login/Senha inválido');
            }
        });
    },

    sigiUp: function () {
        tools.ajax(url.sigUp, login.formLoginUp.serialize(), url.post, function (result) {
            console.log(result);
            if(result.success === 1) {
                $('#modalSignUp').modal('hide');
                var nomeJogador = $('#novoJogador').val();
                console.log(nomeJogador);
                $('#nome').val(nomeJogador);
                tools.messageToast('success', 'Jogador cadastrado com sucesso!');
            } else {
                tools.messageToast('error', 'Erro ao cadastrar jogador');
            }
        });
    },

    validSigin: function () {
        $('#login').validate({
            rules: {
                nome: {
                    required: true,
                    minlength: 3
                },
                senha: {
                    required: true
                }
            },
            submitHandler: function() {
                login.sigin();
                return false;
            }
        });
    },
    
    modalSiginUp: function () {
        $('.sign-up').click(function (e) {
            e.preventDefault();
            $('#modalSignUp').modal('show');
        });
    },

    validSiginUp: function () {
        $('#sign_up').validate({
            rules: {
                nome: {
                    required: true,
                    minlength: 3
                },
                senha: {
                    required: true,
                    minlength : 3
                },
                senha_confirme: {
                    required: true,
                    minlength : 3,
                    equalTo: "#senha"
                }
            },
            submitHandler: function() {
                login.sigiUp();
                return false;
            }
        });
    },
};
