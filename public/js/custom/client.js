/* global tools */

var client = {

    index: function () {
        this.validador();
        this.inputFile();
    },
    
    inputFile: function () {
    	$("#file").fileinput({
        	showUpload: false,
        	language: "pt-BR",
            layoutTemplates: {
                main1: "{preview}\n" +
                "<div class=\'input-group {class}\'>\n" +
                "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>"
            }        
        });
    },

    save: function () {
    	
    	var name = $('#name').val();
    	var email = $('#email').val();
    	var login = $('#login').val();
    	var senha = $('#senha').val();
    	var file = $('#file').prop("files")[0];
    	
    	var formData = new FormData();
    	formData.append("teste", file);
    	
    	console.log(formData);
    	    	
//    	$.post("/cliente/save/client", formData, function(resultado){
//    		alert(123);
//    		tools.print(result);
//        });
    	
//        tools.ajax(, $('#form-save-client').serialize(), "POST", function (result) {
//            console.log(result);
//            tools.print(result);
//        });
    	
    	$.ajax({
            url: "/cliente/save/client",
            type: "POST",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (resultado) {
            	console.log(resultado)
            },
            error: function (resultado) {
            	console.log(resultado)
            }
            
        });
    },

    validador: function () {
        $('#form-save-client').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true
                },
                login: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 3
                }
            },
            submitHandler: function () {
                client.save();
                return false;
            }
        });

    }
};