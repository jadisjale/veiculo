/**
 * Created by jsj on 27/05/2017.
 */
var caminho = 'http://webfate.esy.es/raxa/';

var url = {

    post:              'POST',
    get:               'GET',
    countJogador:       caminho+'quantidade_jogador.php',
    countEnquete:       caminho+'quantidade_enteque.php',
    sumGol:             caminho+'sum_gol_por_jogador.php',
    sumVitoria:         caminho+'sum_vitoria_by_jogador.php',
    getJogadores:       caminho+'get_all_jogador_menos_logado.php',
    getRaxaByJogador:   caminho+'get_all_raxa.php',

};