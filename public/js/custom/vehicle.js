/* global tools */

var vehicle = {

    message_error: '',

    index: function () {
        this.validador();
        this.maskVehicle();
        this.maskYear();
        this.findBoard();

        $.validator.addMethod('checkTwoValues', function(value, element, params) {
            var value_init = $(element).val();
            var value_end = $("#"+ params).val();
            if (value_end) {
                if (value_init > value_end){
                    return false;
                }
            }
            return true;
        }, "Verifique");
    },

    findBoard: function () {
        $('#board').blur(function () {
            var board = $(this).val();
            tools.ajax("/vehicle/findByBoard", {'board': board}, "POST", function (result) {
                if (result.length !== 0) {
                    $('#board').val('');
                    $('#board').focus();
                    tools.messageToast('info', 'Esta placa já existe no sistema');
                }
            });
        })
    },
    
    maskVehicle: function () {
        $("#board").inputmask({mask: 'AAA-9999'});
    },
    
    maskYear: function () {
        $(".year").inputmask({mask: '9999'});
    },
    
    save: function () {
        tools.ajax("/vehicle/save", $('#form-save-vehicle').serialize(), "POST", function (result) {
            if (result.code === 1) {
                tools.resetForm($('#form-save-vehicle'));
                window.location.href = window.location.protocol + "//" + window.location.hostname + '/vehicle/photo/' + result.data.id;
            } else {
                tools.print(result);
            }
        });
    },

    savePhoto: function () {
        var formData = new FormData();
        formData.append('client_id', '1');
        formData.append('vehicle_id', $('input[name="id_vehicle"]').val());

        var files = $('input[type=file]')[0].files;

        $.each(files, function (k, v) {
            formData.append('image[]', v);
        });

        $.ajax({
            url: '/vehicle/photo',
            data: formData,
            type: 'POST',
            contentType:false,
            cache: false,
            processData:false,
            success: function (result) {
                var json = JSON.parse(result);
                tools.print(json);
            },
            error: function (result) {
                var json = JSON.parse(result);
                tools.print(json);
            }
        });
    },

    validador: function () {
        $('#form-save-vehicle').validate({
            rules: {
            	board: {
                    required: true
                },
                pattern: {
                    required: true
                },
                brand: {
                    required: true
                },
                km_init: {
            	    "checkTwoValues": 'km_now'
                },
                year_fabrication: {
                    "checkTwoValues": 'year_model'
                }
            },
            messages: {
                km_init: {
                    checkTwoValues: 'A km inicial não pode ser maior que a km final.'
                },
                year_fabrication: {
                    checkTwoValues: 'O ano do modelo não pode ser menor que o ano de fabricação'
                }
            },
            submitHandler: function () {
            	vehicle.save();
                return false;
            }
        });
    },

    validadorPhoto: function () {
        $('#form-save-vehicle-photo').validate({
            rules: {
                'photos[]': {
                    required: true
                }
            },
            submitHandler: function () {
                vehicle.savePhoto();
                return false;
            }
        });
    },

    indexList: {

        table: '',

        index: function () {
            this.initTable();
            this.loadTable();
        },

        initTable: function () {
            vehicle.indexList.table = $('#list_vehicles').DataTable({
                "language": {
                    "url": "/vendors/plugins/jquery-datatable/pt-br.json"
                }
            });
        },

        loadTable: function () {
            tools.ajax("/vehicle/list", null, "POST", function (result) {
                if (result.code === 1) {
                    var data = result.data;
                    $.each(data, function (key, value) {
                        vehicle.indexList.table.row.add( [
                            value.board,
                            value.brand,
                            value.pattern,
                            value.year_fabrication,
                            '<a href="/vehicle"><label class="label label-primary">Editar Informações</label></a>',
                            '<a href="/vehicle/photo/' + value.id + '"><label class="label label-primary">Fotos</label></a>',
                        ] ).draw( false );
                    });
                } else {
                    tools.print(result);
                }
            });
        }
    }


};