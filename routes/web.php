<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/client')->group(function () {

    Route::get('/', 'ViewController@client')->name('client');

});

Route::prefix('/vehicle')->group(function () {

    Route::get('/', 'ViewController@vehicle')->name('vehicle');
    Route::get('/update/{id}', 'VehicleController@vehicleUpdate')->name('vehicle');
    Route::get('/view_file/{id_vehicle}', 'VehicleController@viewFile')->name('vehicle'); //para q serve testeq
    Route::get('/photo/{id_vehicle}', 'ViewController@vehiclePhoto')->name('photo');
    Route::get('/list', 'ViewController@listVehicle')->name('list');

    Route::post('/save', 'VehicleController@store');
    Route::post('/list', 'VehicleController@listVehicle');
    Route::post('/findByBoard', 'VehicleController@findByBoard');
    Route::post('/photo', 'VehicleController@photo')->name('photo');
});